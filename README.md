# Szkic raportu dla WSPA w LaTeX

## Jak uruchomić?

Szkic jest zoptymalizowany pod kątem platformy Overleaf, ale powinien też działać poza nią. Wymaga posiadania wymienionych w pliku `main.tex` pakietów oraz kompilatora LuaLaTeX.

**Proszę zmienić kompilator na LuaLaTeX.**

## Informacje

Szkic powstał w celu ułatwienia pisania raportów i sprawozdań na uczelni Wyższa Szkoła Przedsiębiorczości i Administracji w Lublinie. Szkic bazuje na wzorze pracy dyplomowej na wydziale WEII na Politechnice Lubelskiej, który został udostępniony w formie otwartej: [LINK](https://codeberg.org/cichy1173/pollub_dyplom_latex).